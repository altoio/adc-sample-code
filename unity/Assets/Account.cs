﻿using System.Collections;
using System.Collections.Generic;
// Here we import the Netherum.JsonRpc methods and classes.
using Nethereum.ABI.Encoders;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Contracts;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Hex.HexTypes;
using Nethereum.JsonRpc.Client;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.RPC.Eth.Transactions;
using Nethereum.Signer;
using Nethereum.Util; 
using UnityEngine;
using UnityEngine.UI;

public class Account : MonoBehaviour 
{
	public Text		  textSetDNAStatus;

	public InputField inputWallet;
	public InputField inputPrivateKey;
	public InputField inputSetDNAItemID;
	public InputField inputSetDNADNA;

	public InputField inputGetDNAItemID;
	public InputField inputGetDNAAddress;
	public InputField inputGetDNADNA;


	// ItemManager ABI
	private string ABI = "[ { \"constant\": true, \"inputs\": [], \"name\": \"ceoAddress\", \"outputs\": [ { \"name\": \"\", \"type\": \"address\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [ { \"name\": \"_newCEO\", \"type\": \"address\" } ], \"name\": \"setCEO\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [], \"name\": \"unpause\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": true, \"inputs\": [], \"name\": \"paused\", \"outputs\": [ { \"name\": \"\", \"type\": \"bool\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [], \"name\": \"withdrawBalance\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [], \"name\": \"pause\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": true, \"inputs\": [ { \"name\": \"_address\", \"type\": \"address\" } ], \"name\": \"hasAccess\", \"outputs\": [ { \"name\": \"\", \"type\": \"bool\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [ { \"name\": \"_address\", \"type\": \"address\" }, { \"name\": \"_isAllowed\", \"type\": \"bool\" } ], \"name\": \"setAccess\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"inputs\": [ { \"name\": \"_storageAddress\", \"type\": \"address\" } ], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"constructor\" }, { \"payable\": true, \"stateMutability\": \"payable\", \"type\": \"fallback\" }, { \"anonymous\": false, \"inputs\": [ { \"indexed\": false, \"name\": \"id\", \"type\": \"uint256\" } ], \"name\": \"ItemCreated\", \"type\": \"event\" }, { \"anonymous\": false, \"inputs\": [], \"name\": \"Pause\", \"type\": \"event\" }, { \"anonymous\": false, \"inputs\": [], \"name\": \"Unpause\", \"type\": \"event\" }, { \"constant\": true, \"inputs\": [ { \"name\": \"_itemId\", \"type\": \"uint256\" } ], \"name\": \"itemExists\", \"outputs\": [ { \"name\": \"\", \"type\": \"bool\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [], \"name\": \"createItem\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [ { \"name\": \"_itemId\", \"type\": \"uint256\" }, { \"name\": \"_dna\", \"type\": \"uint256\" } ], \"name\": \"setDNA\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [ { \"name\": \"_itemId\", \"type\": \"uint256\" }, { \"name\": \"_game\", \"type\": \"address\" }, { \"name\": \"_token\", \"type\": \"uint256\" }, { \"name\": \"_dna\", \"type\": \"uint256\" } ], \"name\": \"setMutableDNA\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" }, { \"constant\": true, \"inputs\": [], \"name\": \"getItemCount\", \"outputs\": [ { \"name\": \"\", \"type\": \"uint256\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": true, \"inputs\": [ { \"name\": \"_itemId\", \"type\": \"uint256\" }, { \"name\": \"_game\", \"type\": \"address\" } ], \"name\": \"getDNA\", \"outputs\": [ { \"name\": \"\", \"type\": \"uint256\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": true, \"inputs\": [ { \"name\": \"_itemId\", \"type\": \"uint256\" }, { \"name\": \"_game\", \"type\": \"address\" }, { \"name\": \"_token\", \"type\": \"uint256\" } ], \"name\": \"getMutableDNA\", \"outputs\": [ { \"name\": \"\", \"type\": \"uint256\" } ], \"payable\": false, \"stateMutability\": \"view\", \"type\": \"function\" }, { \"constant\": false, \"inputs\": [ { \"name\": \"_count\", \"type\": \"uint256\" } ], \"name\": \"createItems\", \"outputs\": [], \"payable\": false, \"stateMutability\": \"nonpayable\", \"type\": \"function\" } ]";

	/// ItemManager Address
	private string contractAddress = "0xb0197478820eb500355a5b747600db69f03ea9f7";

	/// We're using Infura
	public string url = "https://ropsten.infura.io";

	/// Called when Set DNA button is pressed
	public void OnSetDNA()
	{
		StartCoroutine(SetDNA());
	}

	/// Called when Get DNA button is pressed
	public void OnGetDNA()
	{
		StartCoroutine(GetDNA());
	}

	/// Sets the DNA based on the input fields
	/// 1. Estimate Gas parameter
	/// 2. Send transaction
	/// 3. Wait for receipt
	private IEnumerator SetDNA()
	{
		string wallet = inputWallet.text;
		string privateKey = inputPrivateKey.text;

		var contract = new Contract(null, ABI, contractAddress);
        var function = contract.GetFunction("setDNA");

		EthEstimateGasUnityRequest estimateRequest = new EthEstimateGasUnityRequest(url);
        TransactionInput estimateInput = function.CreateTransactionInput(wallet, inputSetDNAItemID.text, inputSetDNADNA.text);
        yield return estimateRequest.SendRequest(estimateInput);
        if (estimateRequest.Exception != null)
        {
			Debug.Log("Exception");
            yield break;
		}

		Debug.Log("Gas: " + estimateRequest.Result.Value);

		var req = new TransactionSignedUnityRequest(url, privateKey, wallet);

		var callInput = function.CreateTransactionInput(
			wallet, 
			estimateRequest.Result,
			new HexBigInteger("0x0"),
			1, 
			inputSetDNADNA.text);
		yield return req.SignAndSendTransaction(callInput);

		textSetDNAStatus.text = "waiting";

		var receiptRequest = new EthGetTransactionReceiptUnityRequest(url);

		bool done = false;
		while (!done)
		{
			yield return receiptRequest.SendRequest(req.Result);

			if (receiptRequest.Result != null)
				done = true;

			yield return new WaitForSeconds(1.0f);
		}

		textSetDNAStatus.text = "done";
        
	}

	/// Gets the DNA based on the input fields
	private IEnumerator GetDNA()
    {
		var req = new EthCallUnityRequest(url);
        var contract = new Contract(null, ABI, contractAddress);
        var function = contract.GetFunction("getDNA");
        var callInput = function.CreateCallInput(inputGetDNAItemID.text, inputGetDNAAddress.text);
        var blockParameter = Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest();
        yield return req.SendRequest(callInput, blockParameter);
        
		inputGetDNADNA.text = req.Result;
    }
}
